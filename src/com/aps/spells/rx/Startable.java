package com.aps.spells.rx;

public interface Startable {

	public void start();
}
