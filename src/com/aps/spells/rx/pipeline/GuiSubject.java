package com.aps.spells.rx.pipeline;

import io.reactivex.Observer;
import io.reactivex.subjects.Subject;

public interface GuiSubject<T> {

	Subject<T> getSubject();

	void subscribe(Observer<T> observer);

	void update();
}
