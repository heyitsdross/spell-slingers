package com.aps.spells.rx.pipeline;

import org.apfloat.Apfloat;

import com.aps.spells.gui.component.ImageBarDisplay;

public class ImageBarUpdater extends GuiUpdater<Apfloat> {

	private ImageBarDisplay imageBarDisplay;

	public ImageBarUpdater(ImageBarDisplay toUpdate) {
		this.imageBarDisplay = toUpdate;
	}

	@Override
	public void onNext(Apfloat newValue) {

		// TODO 2nd + 4th WriteableImage args must add to full image height dimension, same for 1st, 3rd, and width.

	}

}
