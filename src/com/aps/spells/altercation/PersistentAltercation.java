package com.aps.spells.altercation;

/**
 * Indicates an altercation (buff or effect) which is saveable and loadable between sessions.
 *
 */
public interface PersistentAltercation {

	AltercationState mapToState();
}
