package com.aps.spells.altercation;

import org.apfloat.Apfloat;

public interface Applicable {

	Apfloat apply(Apfloat input);
}
