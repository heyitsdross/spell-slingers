package com.aps.spells.altercation;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.effect.Effect;
import com.aps.spells.data.save.Identifiable;
import com.aps.spells.rx.pipeline.ContextReferential;

/**
 * Any sort of change to the game state, whether instantaneous or not.
 *
 * Direct subclasses are regarded as instantaneous, one-and-done altercations, whereas {@link Effect}s are toggleable but passive and self-uninteractive,
 * and {@link Buff}s as toggleable and applicable when appropriate.
 *
 */
public abstract class Altercation implements Identifiable, ContextReferential {

	protected static final Logger log = LoggerFactory.getLogger(Altercation.class);

	protected UUID id = UUID.randomUUID();

	@Override
	public UUID getId() {
		return id;
	}
}
