package com.aps.spells.altercation.effect;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.altercation.Altercation;
import com.aps.spells.altercation.PersistentAltercation;
import com.aps.spells.altercation.Toggleable;
import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.data.save.altercation.effect.EffectState;
import com.aps.spells.rx.pipeline.ContextReferential;

/**
 * Represents the various passive, continuous altercations to game-state
 * which affect one or more players.
 *
 * <p>For applicable, passive altercations, see {@link Buff}. For instantaneous altercations, see all direct
 * subclasses of {@link Altercation}.</p>
 */
public abstract class Effect extends Altercation implements Toggleable, ContextReferential, PersistentAltercation {

	protected static final Logger log = LoggerFactory.getLogger(Effect.class);

	public abstract void proc();

	@Override
	public boolean isActive() {
		return playerStats.get().hasActiveEffect(EffectType.fromClass(this.getClass()));
	}

	@Override
	public boolean isToActivate() {
		return !playerStats.get().hasActiveEffect(EffectType.fromClass(this.getClass()));
	}

	@Override
	public boolean isToDeactivate() {
		return playerStats.get().hasActiveEffect(EffectType.fromClass(this.getClass()));
	}

	@Override
	public abstract void activate();

	@Override
	public void deactivate() {
		playerStats.get().removeEffect(this);
	}

	@Override
	public abstract EffectState mapToState();

	@Override
	public UUID getId() {
		return id;
	}
}
