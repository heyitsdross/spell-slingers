package com.aps.spells.altercation.effect;

import static org.apfloat.ApfloatMath.pow;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import com.aps.spells.altercation.Altercation;
import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.entity.spell.EssenceGatherSpell;
import com.aps.spells.entity.spell.SpellType;
import com.aps.spells.rx.pipeline.ContextReferential;

/**
 * Represents essence gain achieved via left clicking or the automatic gains
 * unlocked when the {@link EssenceGatherSpell} hits the appropriate level.
 */
public class EssenceGatherAltercation extends Altercation implements ContextReferential {

	public static enum GatherMode {
		CLICK, CAST;
	}

	private final GatherMode gatherMode;

	private Apfloat power = ApfloatMath.max(
		new Apfloat("100", 15).add(
				pow(playerStats.get().getSpellMastery(SpellType.ESSENCE_GATHER).getCastCount(), new Apfloat("1.9", 15))
			.multiply(playerStats.get().getSpellMastery(SpellType.ESSENCE_GATHER).getLevel().divide(new Apfloat("10", 15))))
			.divide(new Apfloat("10", 15)),
		Apfloat.ONE);

	public EssenceGatherAltercation(final GatherMode gatherMode) {
		this.gatherMode = gatherMode;
	}

	public EssenceGatherAltercation(final GatherMode gatherMode, final Apfloat power) {
		this.gatherMode = gatherMode;
		this.power = power;
	}

	public Apfloat getSpellPower() {
		return power;
	}

	public void setPower(Apfloat spellPower) {
		this.power = spellPower;
	}

	@Override
	public String toString() {
		return new StringBuffer("Essence Gather Effect ")
				.append("isFromClick: ").append(gatherMode)
				.toString();
	}

	public void proc() {

		ManaStore essenceStore = playerStats.get().getManaStore(ManaColor.ESSENCE);

		essenceStore.getManaAmountLock().readWriteLock(() -> {
			Apfloat currentAmount = essenceStore.getAmount();

			if (gatherMode.equals(GatherMode.CLICK)) {

				essenceStore.setAmount(currentAmount.add(
					 Buff.applyAll(BuffTopic.from(ManaColor.ESSENCE),
							 power.divide(new Apfloat("4", 15)))));

			} else if (gatherMode.equals(GatherMode.CAST)) {

				essenceStore.setAmount(currentAmount.add(
					 Buff.applyAll(BuffTopic.from(ManaColor.ESSENCE),
							 new Apfloat("100").add(ApfloatMath.pow(power, new Apfloat("1.3", 15))))));
			}

			log.debug("Add {} to essence", essenceStore.getAmount().subtract(currentAmount).toString(true));
		});
	}
}
