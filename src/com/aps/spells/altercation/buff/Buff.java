package com.aps.spells.altercation.buff;

import java.util.List;
import java.util.SortedSet;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.Altercation;
import com.aps.spells.altercation.Applicable;
import com.aps.spells.altercation.PersistentAltercation;
import com.aps.spells.altercation.Toggleable;
import com.aps.spells.altercation.effect.Effect;
import com.aps.spells.data.save.altercation.buff.BuffState;

/**
 * Indicates a non-instantaneous, applied-as-needed {@link Altercation}.
 *
 * <p>
 * For one-and-done altercations, see the direct subclasses of <code>Altercation</code>.
 *  For continuous altercations which are uninteractive, see {@link Effect}s.
 *  </p>
 */
public abstract class Buff extends Altercation implements Toggleable, PersistentAltercation, Applicable {

	@Override
	public abstract BuffState mapToState();

	/**
	 * Returns the relevant topic for this buff instance.
	 *
	 * <p>All buff instances of the same type and configuration
	 * should always return the same topic type.</p>
	 *
	 * @return the one relevant topic that this buff applies.
	 */
	public abstract BuffTopic getTopic();

	@Override
	public void activate() {
		playerStats.get().addBuff(this);
	}

	@Override
	public void deactivate() {
		playerStats.get().removeBuff(this);
	}

	@Override
	public boolean isActive() {
		return playerStats.get().hasActiveBuff(id);
	}

	/**
	 * Searches the current player statistic object for buffs implementing one
	 * of the applicable topics, and applies them to the input value.
	 * @param topics to find active effects for.
	 * @param input to be modified
	 * @return the amount, after all relevant buffs have been applied.
	 */
	public static final Apfloat applyAll(SortedSet<BuffTopic> topics, Apfloat input) {

		List<Buff> buffs = playerStats.get().getRelevantBuffs(topics);
		buffs.sort((b1, b2) -> b1.getTopic().compareTo(b2.getTopic()));

		Apfloat result = input;
		for (Buff buff : buffs) {
			result = buff.apply(result);
		}

		return result;
	}

}
