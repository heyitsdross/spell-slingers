package com.aps.spells.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.rx.RxSchedulers;
import com.aps.spells.rx.pipeline.ContextReferential;

import io.reactivex.Completable;
import javafx.application.Application;
import javafx.stage.Stage;

public final class SpellSlingerApplication extends Application implements ContextReferential {

	private static final Logger log = LoggerFactory.getLogger(SpellSlingerApplication.class);

	public SpellSlingerApplication() {
	}

	/**
	 * Prepare FX stage and start the application.
	 */
	public void startApplication(final String[] args) throws Exception {
		launch(args);
	}

	public static void Exce(ArithmeticException ex) {
	    System.out.println("Arithmetic");
	}

	public static void Exce(Exception ex) {
	    System.out.println("Exception");
	}
	@Override
	public void start(final Stage primaryStage) throws Exception {

		log.debug("Loading game...");

		Completable.fromRunnable(() -> saveFileManager.get().loadGame())
			.subscribeOn(RxSchedulers.saveLoad())
			.doOnError(t -> log.error(t.toString()))
			.blockingAwait();

		GameContext.get().getGuiContext().initializeGui(primaryStage);
	}
}
