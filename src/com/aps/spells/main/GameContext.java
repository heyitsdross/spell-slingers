package com.aps.spells.main;

import com.aps.spells.data.PlayerStatistics;
import com.aps.spells.data.SaveFileManager;
import com.aps.spells.gui.GuiContext;
import com.aps.spells.rx.RxContext;

/**
 * Main application state object. Holds various other object references, such as player
 * state, save file managers, etc.
 */
public final class GameContext {

	private static final GameContext instance = new GameContext();

	private final SaveFileManager saveFileManager = new SaveFileManager();

	private final RxContext rxContext = new RxContext();

	private PlayerStatistics playerStatistics;

	/**
	 * Contains the entire GUI, their startup logic, etc.
	 */
	private GuiContext guiContext = new GuiContext();

	private GameContext() {
		// Only allowable singleton instance.
	}

	public PlayerStatistics getPlayerStats() {
		return playerStatistics;
	}

	public void setPlayerStats(PlayerStatistics playerStatistics) {
		this.playerStatistics = playerStatistics;
	}

	public SaveFileManager getSaveFileManager() {
		return saveFileManager;
	}

	public RxContext getRxContext() {
		return rxContext;
	}

	public GuiContext getGuiContext() {
		return guiContext;
	}

	public void setGuiContext(GuiContext guiContext) {
		this.guiContext = guiContext;
	}

	public static GameContext get() {
		return instance;
	}
}
