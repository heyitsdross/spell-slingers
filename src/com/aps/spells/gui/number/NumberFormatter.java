package com.aps.spells.gui.number;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apfloat.Apfloat;

/**
 * Determines how to write numbers to the interface.
 * For small numbers under a certain universal value, will simply
 * print the number without decimal places.
 */
public class NumberFormatter {

	private NumberFormat bigNumberFormat;

	private static final NumberFormat smallNumberFormat = new DecimalFormat("####0");

	/**
	 * The point where the chosen big number format is preferred over the small one.
	 */
	private static final Apfloat bigNumberFloor = new Apfloat("1000000", 15);

	/**
	 * Creates a new number formatter object with the desired big number format.
	 * @param bigNumberFormat to use when formatting a number over the "big number" threshold.
	 */
	public NumberFormatter(NumberFormat bigNumberFormat) {
		this.bigNumberFormat = bigNumberFormat;
	}

	/**
	 * Prints the provided number in either the universal small number
	 * format if the number is under the cap, or the provided one
	 * otherwise.
	 * @param number to format
	 * @return the formatted number
	 */
	public String format(Apfloat number) {

		return bigNumberFloor.compareTo(number) <= 0
			? bigNumberFormat.format(number)
			: smallNumberFormat.format(number);
	}

}
