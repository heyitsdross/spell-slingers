package com.aps.spells.gui;

import java.util.stream.Stream;

import com.aps.spells.entity.spell.SpellType;

/**
 * Stores the relation between a {@link SpellType} and an image file path
 * containing the spell's spell icon.
 */
public enum SpellTypeIcon {
	EXPERIMENT("experiment-128-128.png"),
	VIVIFY("vivify_s.png"),
	FOCUS_ERUPT("focus/focus_crimson_s.png"),
	FOCUS_SURGE("focus/focus_myrtle_s.png"),
	FOCUS_FIXATE("focus/focus_lapis_s.png"),
	FOCUS_VILIFY("focus/focus_onyx_s.png"),
	FOCUS_BASK("focus/focus_alabaster_s.png"),
	FOCUS_NEGATE("focus/focus_void_s.png"),
	FOCUS_INVIGORATE("focus/focus_icterine_s.png"),
	ESSENCE_GATHER("gather_essence_s.png"),
	NULL("../spell-slot/empty.png");

	private final String iconFilePath;

	private SpellTypeIcon(String iconFilePath) {
		this.iconFilePath = iconFilePath;
	}

	public String getIconFilePath() {
		return iconFilePath;
	}

	public static String fromSpellType(SpellType type) {
		return Stream.of(SpellTypeIcon.values())
			.filter(x -> x.name().equalsIgnoreCase(type.name()))
			.map(s -> s.getIconFilePath())
			.findFirst()
			.orElseThrow(() -> new IllegalStateException("SpellTypeIconRelation missing matching SpellType entry."));
	}
}
