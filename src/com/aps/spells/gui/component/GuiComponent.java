package com.aps.spells.gui.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.rx.pipeline.ContextReferential;

import javafx.scene.Parent;

public abstract class GuiComponent implements ContextReferential {

	protected static final Logger log = LoggerFactory.getLogger(GuiComponent.class);

	/**
	 * Performs sub-object initialization, formats and style sub-components,
	 * and attaches relevant reactive components.
	 */
	public abstract void initialize();

	/**
	 * Gets the {@link Parent} responsible for holding all other pieces of this
	 * GUI component.
	 */
	public abstract Parent getContainer();
}
