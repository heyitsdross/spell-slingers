package com.aps.spells.gui;

import java.util.stream.Stream;

/**
 * Provides a globally referenceable list of component ids.
 */
public enum ComponentId {
	EQUIPPED_SPELL_CONTAINER("equipped-spell-container"),
	SPELL_BOOK_CONTAINER("spellbook");

	private String id;

	private ComponentId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public static ComponentId fromString(String idName) {
		return Stream.of(ComponentId.values())
				.filter(s -> s.getId().equals(idName))
				.findFirst().orElse(null);
	}
}
