package com.aps.spells.gui;

import java.util.stream.Stream;

import com.aps.spells.entity.spell.SpellType;

/**
 * Working solution for storing spell descriptions until a suitable
 * file-based approach is found and implemented.
 * @param spellType of the spell who has the description
 * @param description of the spell type
 */
public enum SpellDescription {
	FOCUS_SURGE(SpellType.FOCUS_SURGE, "Consume increasing percentages of essence (ends at 100%) over the next 3.2 seconds, granting slightly more myrtle mana."),
	FOCUS_ERUPT(SpellType.FOCUS_ERUPT, "Instantly consume all essence to provide crimson mana equal to the amount consumed plus a small multiplier, which degrades in quality as spell level increases."),
	FOCUS_FIXATE(SpellType.FOCUS_FIXATE, "Observes non-lapis mana changes for a time (based on spell level), then adds lapis mana based on color unique multipliers."),
	FOCUS_VILIFY(SpellType.FOCUS_VILIFY, "Instantly grants a large ratio of onyx mana, but significantly reduces your essence generation for the next 10 seconds."),
	FOCUS_BASK(SpellType.FOCUS_BASK, "Consumes essence on channel. When the channel is released, gain alabaster mana equal to the total essence spent multiplied by a channel duration based multiplier."),
	FOCUS_NEGATE(SpellType.FOCUS_NEGATE, "Converts all non-void mana into void mana based on the amounts consumed."),
	FOCUS_INVIGORATE(SpellType.FOCUS_INVIGORATE, "Slowly gathers icterine mana over a long duration, then proeeds to a long cooldown."),
	ESSENCE_GATHER(SpellType.ESSENCE_GATHER, "Channels essence through your body, granting the raw energy required to yield mana. As your skills increase, you may be able to automatically gather essence."),
	EXPERIMENT(SpellType.EXPERIMENT, "Do random stuff to try to figure something new out. May unlock a new spell."),
	VIVIFY(SpellType.VIVIFY, "Cast to instantly regenerate a small portion of health, or channel to gradually heal over the duration."),
	NULL(SpellType.NULL, "Drag another spell here to equip it.");


	private SpellType spellType;
	private String description;

	private SpellDescription(SpellType spellType, String description) {
		this.spellType = spellType;
		this.description = description;
	}

	public static SpellDescription fromSpellType(SpellType spellType) {
		return Stream.of(SpellDescription.values())
				.filter(sd -> sd.getSpellType().equals(spellType))
				.findFirst().get();
	}

	public SpellType getSpellType() {
		return spellType;
	}

	public String getDescription() {
		return description;
	}
}
