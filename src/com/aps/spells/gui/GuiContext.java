package com.aps.spells.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aps.spells.gui.component.SpellSlingerWindow;

import javafx.stage.Stage;

/**
 * Main GUI class responsible for storing the entire GUI and some
 * initialization logic. Also registers which keys are currently active.
 */
public final class GuiContext {

	private static final Logger log = LoggerFactory.getLogger(GuiContext.class);

	private SpellSlingerWindow mainWindow;

	private final PlatformHelper platformHelper = new PlatformHelper();

	public void initializeGui(final Stage mainStage) {
		log.trace("Beginning GUI construction.");
		mainWindow = new SpellSlingerWindow(mainStage);
		mainWindow.initialize();
	}

	/**
	 * Runs a piece of code or sets it up to be run on the JavaFX thread.
	 * @param treatment to perform or schedule on the JavaFX thread.
	 */
	public void run(Runnable treatment) {
		platformHelper.run(treatment);
	}

	public SpellSlingerWindow getMainWindow() {
		return mainWindow;
	}
}
