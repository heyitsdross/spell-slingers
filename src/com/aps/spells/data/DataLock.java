package com.aps.spells.data;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

/**
 * Utility class which wraps thread locking operations.
 */
public class DataLock {

	private final ReadWriteLock lock = new ReentrantReadWriteLock(true);

	public static DataLock create() {
		return new DataLock();
	}

	private DataLock() {
	}

	public <T> void writeLock(Supplier<T> action) {
		lock.writeLock().lock();
		try {
			action.get();
		} finally {
			lock.writeLock().unlock();
		}
	}

	public void writeLock(Runnable action) {
		lock.writeLock().lock();
		try {
			action.run();
		} finally {
			lock.writeLock().unlock();
		}
	}

	public void readWriteLock(Runnable action) {
		lock.writeLock().lock();
		lock.readLock().lock();

		try {
			action.run();
		} finally {
			lock.writeLock().unlock();
			lock.readLock().unlock();
		}
	}

	public <T> T readWriteLock(Supplier<T> action) {
		lock.writeLock().lock();
		lock.readLock().lock();

		try {
			return action.get();
		} finally {
			lock.writeLock().unlock();
			lock.readLock().unlock();
		}
	}

	public <T> T readLock(Supplier<T> action) {
		lock.readLock().lock();
		try {
			return action.get();
		} finally {
			lock.readLock().unlock();
		}
	}

	public <T> void readLock(Runnable action) {
		lock.readLock().lock();
		try {
			action.run();
		} finally {
			lock.readLock().unlock();
		}
	}
}
