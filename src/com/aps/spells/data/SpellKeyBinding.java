package com.aps.spells.data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.aps.spells.entity.spell.Spell;

import javafx.scene.input.KeyCode;

/**
 * Stores information about equipped spells and which key is used to activate them.
 * Also stores which keys can be used to store spells (hint: number keys 1-0).
 */
public class SpellKeyBinding implements Serializable {
	private static final long serialVersionUID = 6970969554996339172L;

	public static final List<KeyCode> allowedKeyCodes = Arrays.asList(
			KeyCode.DIGIT1, KeyCode.DIGIT2, KeyCode.DIGIT3,
			KeyCode.DIGIT4, KeyCode.DIGIT5, KeyCode.DIGIT6,
			KeyCode.DIGIT7, KeyCode.DIGIT8, KeyCode.DIGIT9,
			KeyCode.DIGIT0);

	private final KeyCode keyCode;
	private Class<? extends Spell> spellClass;

	public SpellKeyBinding(Class<? extends Spell> spellClass, KeyCode assignedKey) {
		this.spellClass = spellClass;
		this.keyCode = assignedKey;
	}

	public Class<? extends Spell> getSpellClass() {
		return spellClass;
	}

	public void setSpellClass(Class<? extends Spell> spellClass) {
		this.spellClass = spellClass;
	}

	public KeyCode getKeyCode() {
		return keyCode;
	}

	public void swapWith(SpellKeyBinding target) {
		Class<? extends Spell> targetClass = target.getSpellClass();

		target.setSpellClass(spellClass);
		this.setSpellClass(targetClass);
	}

	@Override
	public String toString() {
		return new StringBuffer().append("SpellKeyBinding: ")
				.append("KeyCode: ").append(keyCode.toString()).append("\n")
				.append("Spell Class: ").append(spellClass).append("\n")
				.toString();
	}
}
