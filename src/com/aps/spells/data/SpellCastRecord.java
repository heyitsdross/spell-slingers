package com.aps.spells.data;

import java.io.Serializable;
import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apfloat.Apfloat;

import com.aps.spells.data.save.CooldownState;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.spell.SpellType;

/**
 * Records a previous spell cast. Enables accurate copies.
 * <p>Copyable spells must implement the {@link Copyable} interface and appropriately specify which parameters in
 * what types are necessary.</p>
 */
public class SpellCastRecord implements Serializable {
	private static final long serialVersionUID = -2596365457033288071L;

	private UUID id = UUID.randomUUID();

	private SpellType spellType;
	private List<ManaCost> paidCosts = new ArrayList<>();
	private Apfloat spellPower;

	/**
	 * Stores exactly when the spell was cast. Stored in 64 bit UNIX time.
	 */
	private long castTimestamp;

	/**
	 * Tells how long the spell was channeled for.
	 */
	private Duration channelDuration;

	/**
	 * Copies the exact cooldown, including duration, interval, and so on.
	 */
	private CooldownState cooldown;


	/**
	 * Stores copy-relevant statistics for later replayability.
	 */
	private Map<RecordParameterKey, Serializable> inputParameters = new LinkedHashMap<>();

	public SpellCastRecord(SpellType spellType) {
		this.spellType = spellType;
	}

	public void addPaidCost(ManaCost cost) {
		paidCosts.add(cost);
	}

	public void addParameter(RecordParameterKey key, Serializable value) {
		inputParameters.put(key, value);
	}

	public SpellType getSpellType() {
		return spellType;
	}

	public List<ManaCost> getPaidCosts() {
		return paidCosts;
	}

	public long getCastTimestamp() {
		return castTimestamp;
	}

	public void setCastTimestamp(long castTimestamp) {
		this.castTimestamp = castTimestamp;
	}

	public Duration getChannelDuration() {
		return channelDuration;
	}

	public void setChannelDuration(Duration channelDuration) {
		this.channelDuration = channelDuration;
	}

	public CooldownState getCooldownState() {
		return cooldown;
	}

	public void setCooldownState(CooldownState cooldown) {
		this.cooldown = cooldown;
	}

	public Serializable getInputParameter(RecordParameterKey key) {
		return inputParameters.get(key);
	}
	public Map<RecordParameterKey, Serializable> getInputParameters() {
		return inputParameters;
	}

	public UUID getId() {
		return id;
	}

	public Apfloat getSpellPower() {
		return spellPower;
	}

	public void setSpellPower(Apfloat spellPower) {
		this.spellPower = spellPower;
	}

	@Override
	public String toString() {
		return "SpellCastRecord [id=" + id + ", spellType=" + spellType + ", paidCosts=" + paidCosts
				+ ", castTimestamp=" + castTimestamp + ", channelDuration=" + channelDuration + ", cooldown=" + cooldown
				+ ", inputParameters=" + inputParameters + "]";
	}
}
