package com.aps.spells.data.save;

import java.io.Serializable;

/**
 * Indicates an object requires custom logic to properly save relevant information.
 * <br>
 * <i>To make save method mandatory, override abstract void save() in an abstract class or non default
 * in an extended interface.</i>
 */
public interface CustomSaveable extends Serializable {

	public default void save() {
		// No op to make method optional.
	}
}
