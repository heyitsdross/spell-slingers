package com.aps.spells.data.save;

/**
 * Indicates object requires custom logic to be able to reload.
 */
public interface CustomLoadable {

	/**
	 * Reconstructs the object from serializable data.
	 */
	public void load();
}
