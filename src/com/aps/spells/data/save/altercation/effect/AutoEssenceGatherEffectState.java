package com.aps.spells.data.save.altercation.effect;

import java.util.UUID;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.effect.AutoEssenceGatherEffect;

/**
 * Serialization-friendly buff storage for {@link AutoEssenceGatherEffect}.
 */
public class AutoEssenceGatherEffectState extends EffectState {
	private static final long serialVersionUID = -659232311722686269L;

	private Apfloat spellPower;

	public AutoEssenceGatherEffectState(UUID id, Apfloat spellPower) {
		this.id = id;
		this.spellPower = spellPower;
	}

	@Override
	public void load() {
		var autoGatherer = new AutoEssenceGatherEffect(id);
		autoGatherer.update(spellPower);
		autoGatherer.toggle();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((spellPower == null) ? 0 : spellPower.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutoEssenceGatherEffectState other = (AutoEssenceGatherEffectState) obj;
		if (spellPower == null) {
			if (other.spellPower != null)
				return false;
		} else if (!spellPower.equals(other.spellPower))
			return false;
		return true;
	}
}
