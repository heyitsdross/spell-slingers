package com.aps.spells.data.save.altercation.effect;

import java.time.Duration;
import java.util.UUID;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.effect.ManaSurgeEffect;
import com.aps.spells.data.Formula;
import com.aps.spells.entity.spell.focus.FocusSurgeSpell;

/**
 * Saves mana surge buff information.
 */
public final class ManaSurgeEffectState extends EffectState {
	private static final long serialVersionUID = -6627272410721125591L;

	private final Duration remainingDuration;
	private final Formula<Apfloat, Apfloat> paymentFormula;

	public ManaSurgeEffectState(UUID id, Duration remainingDuration, Formula<Apfloat, Apfloat> formula) {
		this.id = id;
		this.remainingDuration = remainingDuration;
		this.paymentFormula = formula;
	}

	@Override
	public void load() {
		ManaSurgeEffect.Builder builder =
				new ManaSurgeEffect.Builder(id, remainingDuration)
					.triggeringCooldownFor(new FocusSurgeSpell());

		if (paymentFormula != null) {
			builder.consumingEssence(paymentFormula);
		}

		builder.build().toggle();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((paymentFormula == null) ? 0 : paymentFormula.hashCode());
		result = prime * result + ((remainingDuration == null) ? 0 : remainingDuration.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ManaSurgeEffectState other = (ManaSurgeEffectState) obj;
		if (paymentFormula == null) {
			if (other.paymentFormula != null)
				return false;
		} else if (!paymentFormula.equals(other.paymentFormula))
			return false;
		if (remainingDuration == null) {
			if (other.remainingDuration != null)
				return false;
		} else if (!remainingDuration.equals(other.remainingDuration))
			return false;
		return true;
	}
}
