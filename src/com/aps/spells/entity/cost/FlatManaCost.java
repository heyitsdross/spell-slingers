package com.aps.spells.entity.cost;

import org.apfloat.Apfloat;

import com.aps.spells.entity.ManaColor;

/**
 * Represents a flat, unchanging quantity of mana that must be paid, ie. 5 Crimson mana.
 */
public class FlatManaCost extends ManaCost {
	private static final long serialVersionUID = 8157340291277054453L;

	private Apfloat amount;

	/**
	 * @param amount finalized flat amount to pay
	 * @param color in which the mana will be paid
	 */
	public FlatManaCost(Apfloat amount, ManaColor color) {
		this.amount = amount;
		this.color = color;
	}

	@Override
	public Apfloat pay() {
		var manaStore = playerStats.get().getManaStore(color);

		return manaStore.getManaAmountLock().readWriteLock(() -> {
			final var oldAmount = manaStore.getAmount();

			log.debug("Paying {} of {} {} mana; {} remains.",
					amount, oldAmount, color.toString(), oldAmount.subtract(amount));

			manaStore.setAmount(oldAmount.subtract(amount));
			return amount;
		});
	}

	@Override
	public boolean isPayable() {
		var manaStore = playerStats.get().getManaStore(color);

		return manaStore.getManaAmountLock().readLock(
				() -> amount.compareTo(manaStore.getAmount()) <= 0);
	}
}
