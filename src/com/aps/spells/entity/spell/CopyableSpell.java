package com.aps.spells.entity.spell;

import com.aps.spells.data.SpellCastRecord;

/**
 * Indicates that the implementing Spell can be copied.
 */
public interface CopyableSpell {

	/**
	 * Signals whether the entity is copyable or not.
	 * @return whether this is copyable or not
	 */
	public default boolean isCopyable() {
		return true;
	}

	/**
	 * Casts a copy of this spell using parameters recorded from a previous cast.
	 */
	public void castCopy(SpellCastRecord copyFrom);
}
