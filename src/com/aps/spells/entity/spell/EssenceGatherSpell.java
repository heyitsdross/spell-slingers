package com.aps.spells.entity.spell;

import static org.apfloat.ApfloatMath.log;
import static org.apfloat.ApfloatMath.pow;

import java.time.Duration;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.effect.AutoEssenceGatherEffect;
import com.aps.spells.altercation.effect.EssenceGatherAltercation;
import com.aps.spells.altercation.effect.EssenceGatherAltercation.GatherMode;
import com.aps.spells.data.BiFormula;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.data.SpellCastRecord;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.entity.NumberFormats;

/**
 * Not actually a "spell", but treated like one. Augments manual and automatic
 * essence gathering.
 */
public class EssenceGatherSpell extends Spell implements CopyableSpell {

	private SpellCastRecord copyInfo;

	/**
	 * log((20 + cast count^2.6 * level))^2
	 */
	private BiFormula<Apfloat, Apfloat, Apfloat> spellPowerFormula =
			(level, castCount) -> pow(log(new Apfloat("20", 15)
						.add(pow(castCount, new Apfloat("2.6", 15)))
						.multiply(level)),
					new Apfloat("2", 15));

	/**
	 * Standard cooldown for this spell. Set to 5 seconds.
	 */
	private Cooldown.Builder standardCooldown = new Cooldown.Builder(SpellType.ESSENCE_GATHER)
			.ofLength(Duration.ofSeconds(5));

	private static final AutoEssenceGatherEffect autoEssenceGatherer = new AutoEssenceGatherEffect();

	public EssenceGatherSpell() {
	}

	@Override
	public void cast() {
		if (!isCastable()) {
			return;
		}
		var mastery = playerStats.get().getSpellMastery(SpellType.from(this));
		spellPower = spellPowerFormula.apply(mastery.getLevel(), mastery.getCastCount());
		log.debug("Spell power is {}", NumberFormats.THREE_SIG_FIG.format(spellPower));

		super.cast();

		procEffects();

		super.gainExperience();

		startCooldown();
		super.recordSpellCast();
	}

	@Override
	public void procEffects() {
		spellRecord.addParameter(RecordParameterKey.SPELL_POWER, spellPower);

		var appliedSpellPower = copyInfo == null ? spellPower
				: (Apfloat) copyInfo.getInputParameter(RecordParameterKey.SPELL_POWER);

		var castEffect = new EssenceGatherAltercation(GatherMode.CAST, appliedSpellPower);

		castEffect.proc();

		autoEssenceGatherer.toggle();
		autoEssenceGatherer.update(spellPower);
	}

	@Override
	public void startCooldown() {

		Cooldown cooldown = Objects.nonNull(copyInfo)
				? copyInfo.getCooldownState().toCooldown()
				: standardCooldown.build();

		spellRecord.setCooldownState(new CooldownState.Builder(cooldown).build());

		cooldown.start();
	}

	@Override
	public Set<String> getCostsReadable() {
		return Collections.singleton("No cost");
	}

	@Override
	public void castCopy(SpellCastRecord copyFrom) {
		copyInfo = copyFrom;
		this.cast();
	}
}
