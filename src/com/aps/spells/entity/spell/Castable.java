package com.aps.spells.entity.spell;

/**
 * Used to express effects which should occur when a spell's costs
 * are paid and the spell begins being cast, but does not come
 * directly into full effect.
 */
public interface Castable {

	public void cast();
	
	public boolean isCastable();
}
