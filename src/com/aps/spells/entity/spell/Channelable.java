package com.aps.spells.entity.spell;

import java.time.Duration;

import com.aps.spells.rx.Stoppable;

public interface Channelable extends Stoppable {

	void channel();
	boolean isChanneling();

	boolean canBeChanneled();

	Duration getChannelDuration();

	@Override
	void stop();
}
