package com.aps.spells.entity.spell;

import java.time.Duration;

import com.aps.spells.data.DataLock;

public abstract class ChanneledSpell extends Spell implements Channelable {

	/**
	 * The number of ticks this channel has been channeling for.
	 */
	protected Duration channelDuration = Duration.ofMillis(0);

	protected boolean isChanneling = false;
	protected DataLock isChannelingLock = DataLock.create();

	protected Duration interval = Duration.ofMillis(100);

	@Override
	public Duration getChannelDuration() {
		return channelDuration;
	}

	public void addToDuration(Duration toAdd) {
		channelDuration = channelDuration.plus(toAdd);
	}

	@Override
	public boolean isChanneling() {
		return isChanneling;
	}

	public DataLock getIsChannelingLock() {
		return isChannelingLock;
	}

	public Duration getInterval() {
		return interval;
	}

	@Override
	public void channel() {
		isChannelingLock.readWriteLock(() -> isChanneling = true);
		spellPowerLatch.countDown();
	}

	@Override
	public void stop() {
		channelDuration = Duration.ofMillis(0);
		isChannelingLock.readWriteLock(() -> isChanneling = false);
	}
}
