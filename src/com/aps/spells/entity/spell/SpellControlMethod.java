package com.aps.spells.entity.spell;

public enum SpellControlMethod {
	SIMPLE_CAST,
	CHANNEL,
	CHANNEL_TO_CAST;
}
