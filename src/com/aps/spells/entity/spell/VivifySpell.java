package com.aps.spells.entity.spell;

import static org.apfloat.Apcomplex.ONE;
import static org.apfloat.ApfloatMath.log;

import java.time.Duration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.HealAltercation;
import com.aps.spells.data.BiFormula;
import com.aps.spells.data.ContentLocked;
import com.aps.spells.data.DataLock;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.data.SpellKeyBinding;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.entity.ContentLock;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.FlatManaCost;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.unlock.UnlockTopic;

import javafx.scene.input.KeyCode;

/**
 * Simple castable / channelable spell. Regenerates health based on mana spent.
 */
public class VivifySpell extends ChanneledSpell implements ContentLocked {

	private static final ContentLock contentLock = new ContentLock.Builder()
			.withCondition(
					UnlockTopic.CURRENT_ICTERINE_MANA,
					mana -> new Apfloat("1000", 15).compareTo(mana) <= 0,
					() -> playerStats.get().getManaStore(ManaColor.ICTERINE).getAmount())
			.withCondition(
					UnlockTopic.CURRENT_MYRTLE_MANA,
					mana -> new Apfloat("1000", 15).compareTo(mana) <= 0,
					() -> playerStats.get().getManaStore(ManaColor.MYRTLE).getAmount())
			.doOnUnlock(() -> playerStats.get().addSpellKeyBinding(new SpellKeyBinding(VivifySpell.class, KeyCode.DIGIT9)))
		.build();

	private Apfloat myrtlePaid = Apfloat.ZERO;
	private DataLock manaPaidLock = DataLock.create();
	private Apfloat icterinePaid = Apfloat.ZERO;

	private static final List<ManaCost> castingCosts = List.of(
			new FlatManaCost(new Apfloat("1500", 15), ManaColor.ICTERINE),
			new PercentCurrentManaCost(new Apfloat("35", 15), ManaColor.ICTERINE),
			new FlatManaCost(new Apfloat("1500", 15), ManaColor.MYRTLE),
			new PercentCurrentManaCost(new Apfloat("35", 15), ManaColor.MYRTLE));

	private static final List<ManaCost> channelCosts = List.of(
			new FlatManaCost(new Apfloat("5", 15), ManaColor.ICTERINE),
			new PercentCurrentManaCost(ONE, ManaColor.ICTERINE),
			new FlatManaCost(new Apfloat("5", 15), ManaColor.MYRTLE),
			new PercentCurrentManaCost(ONE, ManaColor.MYRTLE));

	/**
	 *  (log(1 + myrtlePaid) + 2 * log(1 + icterinePaid)) / 3
	 */
	private static final BiFormula<Apfloat, Apfloat, Apfloat> spellPowerFormula =
			(icterinePaid, myrtlePaid) ->
				log(ONE.add(myrtlePaid))
					.add(log(ONE.add(icterinePaid)).multiply(new Apfloat(2, 15)))
					.divide(new Apfloat(3, 15));

	@Override
	public void channel() {
		if (!canBeChanneled() || !contentLock.isUnlocked()) {
			if (this.isChanneling()) {
				this.stop();
			}
			return;
		}
		spellPowerLatch = new CountDownLatch(1);

		log.debug("Channeling spell. Vivify");

		manaPaidLock.readWriteLock(() -> {
			myrtlePaid = channelCosts.stream()
					.filter(cost -> cost.getColor().equals(ManaColor.MYRTLE))
					.map(cost -> cost.pay())
					.reduce(Apfloat.ZERO, Apfloat::add);

			icterinePaid = channelCosts.stream()
					.filter(cost -> cost.getColor().equals(ManaColor.ICTERINE))
					.map(cost -> cost.pay())
					.reduce(Apfloat.ZERO, Apfloat::add);

		});

		spellPower = spellPowerFormula.apply(icterinePaid, myrtlePaid);

		spellRecord.setSpellPower(spellPower);

		super.channel();

		new HealAltercation(spellPower.divide(new Apfloat("10", 15))).proc();
	}

	@Override
	public void stop() {
		if (!playerStats.get().hasCooldown(SpellType.from(this)) && isChanneling()) {
			this.startCooldown();
		}
		super.stop();
	}

	@Override
	public void cast() {
		if (!isCastable()
				|| !contentLock.isUnlocked()
				|| castingCosts.stream().anyMatch(cost -> !cost.isPayable())) {
			return;
		}

		log.debug("Casting");

		manaPaidLock.readWriteLock(() -> {
			icterinePaid = castingCosts.stream()
				.filter(cost -> !cost.getColor().equals(ManaColor.ICTERINE))
				.map(cost -> cost.pay())
				.reduce(Apfloat.ZERO, Apfloat::add);

			myrtlePaid = castingCosts.stream()
				.filter(cost -> !cost.getColor().equals(ManaColor.MYRTLE))
				.map(cost -> cost.pay())
				.reduce(Apfloat.ZERO, Apfloat::add);
		});

		castingCosts.forEach(spellRecord::addPaidCost);

		var toHeal = spellPowerFormula.apply(icterinePaid, myrtlePaid);
		new HealAltercation(toHeal).proc();

		spellRecord.addParameter(RecordParameterKey.EFFECT_PARAMETER, toHeal);

		this.startCooldown();
		this.recordSpellCast();
	}

	@Override
	public void startCooldown() {
		var cooldown = new Cooldown.Builder(SpellType.from(this))
			.ofLength(Duration.ofMinutes(1))
			.build();

		spellRecord.setCooldownState(new CooldownState.Builder(cooldown).build());

		cooldown.start();
	}

	@Override
	public void procEffects() {
		// unused
	}

	@Override
	public Set<String> getCostsReadable() {
		return Stream.of("(Cast) 1500 + 35% current Icterine mana",
				"(Cast) 1500 + 35% current Myrtle mana",
				"(Channel) 5 + 1% current Myrtle & Icterine mana")
			.collect(Collectors.toCollection(LinkedHashSet::new));
	}

	public static ContentLock getContentLock() {
		return contentLock;
	}

	@Override
	public boolean canBeChanneled() {
		return channelCosts.stream().allMatch(ManaCost::isPayable)
				&& !playerStats.get().hasCooldown(SpellType.VIVIFY);
	}
}
