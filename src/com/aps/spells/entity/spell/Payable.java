package com.aps.spells.entity.spell;

import org.apfloat.Apfloat;

public interface Payable {
	/**
	 * Calculates the amount of cost to be paid, and subtracts from the relevant
	 * resource store.
	 *
	 * @param stats to take payment from.
	 * @return the amount paid.
	 */
	public Apfloat pay();

	/**
	 * Determine if the associated payment can be made.
	 * @param stats to check from
	 * @return if the cost can be paid with the current stats.
	 */
	public boolean isPayable();
}
