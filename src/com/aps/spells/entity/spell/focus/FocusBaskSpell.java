package com.aps.spells.entity.spell.focus;

import static org.apfloat.ApfloatMath.log;
import static org.apfloat.ApfloatMath.pow;

import java.time.Duration;
import java.util.Set;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import com.aps.spells.altercation.ManaFlareAltercation;
import com.aps.spells.altercation.buff.Buff;
import com.aps.spells.altercation.buff.BuffTopic;
import com.aps.spells.data.Formula;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.FlatManaCost;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.ChanneledSpell;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.SpellType;

public class FocusBaskSpell extends ChanneledSpell {

	private Apfloat essencePaid = Apfloat.ZERO;

	private final Set<ManaCost> channelCosts = Set.of(
			new FlatManaCost(playerStats.get().getSpellMastery(SpellType.from(this.getClass())).getLevel(), ManaColor.ESSENCE),
			new PercentCurrentManaCost(new Apfloat("15", 15), ManaColor.ESSENCE));

	private static final Formula<Apfloat, Apfloat> channelExperienceFormula =
			tickNumber -> ApfloatMath.pow(tickNumber, new Apfloat("1.2", 15))
				.divide(new Apfloat("10", 15));

	private static final Formula<Apfloat, Apfloat> magnitudeFormula = channelLength ->
			Apfloat.ONE.add(ApfloatMath.pow(channelLength, new Apfloat("0.4", 15)).divide(new Apfloat("10")));

	@Override
	public void cast() {
		synchronized(this) {
			if (!isCastable()) {
				return;
			}

			log.debug("Casting focus bask.");

			isChannelingLock.readWriteLock(() -> isChanneling = false);


			procEffects();

			channelDuration = Duration.ofMillis(0);

			startCooldown();
		}
	}

	@Override
	public void channel() {
		synchronized(this) {
			if (!canBeChanneled()) {
				if (this.isChanneling()) {
					this.cast();
				}
				return;
			}

			channelCosts.forEach(cost -> {
					essencePaid = essencePaid.add(cost.pay());
					spellRecord.addPaidCost(cost);
				});

			spellPower = new Apfloat("2", 15).multiply(
				pow(log(Apfloat.ONE.add(essencePaid)), new Apfloat("1.4", 15)));

			spellRecord.addParameter(RecordParameterKey.SPELL_POWER, spellPower);

			super.channel();

			this.gainExperience(channelExperienceFormula, new Apfloat(channelDuration.toSeconds()));
		}
	}

	@Override
	public void startCooldown() {
		new Cooldown.Builder(SpellType.from(this.getClass()))
			.ofLength(Duration.ofSeconds(9))
			.build().start();

		super.recordSpellCast();
	}

	@Override
	public void stop() {
		log.debug("Stopping focus bask.");
		spellRecord.setChannelDuration(channelDuration);
	}

	@Override
	public void procEffects() {
		log.debug("Applying mana flare with magnitude {}",
				magnitudeFormula.apply(new Apfloat(channelDuration.toMillis() / 100, 15)));

		new ManaFlareAltercation(
				Buff.applyAll(BuffTopic.from(ManaColor.ALABASTER),
						essencePaid.add(magnitudeFormula.apply(new Apfloat(channelDuration.toMillis() / 100, 15)))),
				ManaColor.ALABASTER).proc();
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("Spell level + 15% current Essence / 0.1s channeled");
	}

	@Override
	public boolean isCastable() {
		return super.isCastable()
				&& isChanneling()
				&& channelDuration.compareTo(Duration.ofMillis(0)) > 0;
	}

	@Override
	public boolean canBeChanneled() {
		return !playerStats.get().hasCooldown(SpellType.from(this))
				&& channelCosts.stream().allMatch(ManaCost::isPayable);
	}
}
