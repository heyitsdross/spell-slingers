package com.aps.spells.entity.spell.focus;

import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import com.aps.spells.altercation.effect.ManaFixateEffect;
import com.aps.spells.data.Formula;
import com.aps.spells.data.RecordParameterKey;
import com.aps.spells.data.SpellCastRecord;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.CopyableSpell;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.entity.spell.SpellType;

/**
 * Lapis focus spell. Watches other mana stores for changes and copies them, adjusted by ratio.
 */
public class FocusFixateSpell extends Spell implements CopyableSpell {

	private SpellCastRecord copyFrom;

	private Cooldown customCooldown = null;

	/**
	 * Duration, in milliseconds, for which the watch effect should persist for.
	 * <i>3000 + 100 * level^0.75</i>
	 */
	private static final Formula<Apfloat, Apfloat> watchDurationFormula = level ->
		new Apfloat("3000", 15)
			.add(new Apfloat("100", 15).multiply(ApfloatMath.pow(level, new Apfloat("0.75", 15))));

	/**
	 * How much each recorded mana change should be multiplied by per color.
	 */
	private static final Map<ManaColor, Apfloat> defaultColorMultipliers =
			Map.of(ManaColor.ESSENCE, new Apfloat("1.55", 15),
					ManaColor.CRIMSON, new Apfloat("1.05", 15),
					ManaColor.VOID, new Apfloat("1.02", 15),
					ManaColor.ICTERINE, new Apfloat("1.09", 15),
					ManaColor.MYRTLE, new Apfloat("1.3", 15),
					ManaColor.ALABASTER, new Apfloat("1.2", 15),
					ManaColor.ONYX, new Apfloat("1.2", 15));

	private static final ManaCost defaultCost =
			new PercentCurrentManaCost(new Apfloat("100", 15), ManaColor.ESSENCE);

	private static ManaFixateEffect fixateEffect = new ManaFixateEffect(defaultColorMultipliers);

	public FocusFixateSpell() {
		// Required to cast normally
	}

	/**
	 * Constructor used to enable copying this spell without causing cooldown race conditions.
	 * @param cooldown to replace the default one with.
	 */
	public FocusFixateSpell withCooldown(Cooldown cooldown) {
		this.customCooldown = cooldown;
		return this;
	}

	@Override
	public boolean isCastable() {
		return super.isCastable()
				&& !playerStats.get().hasActiveEffect(fixateEffect);
	}

	@Override
	public void cast() {
		if (!this.isCastable()) {
			return;
		}

		log.debug("Casting Focus Fixate.");

		// TODO move to Spell?
		if (Objects.nonNull(copyFrom) && !copyFrom.getPaidCosts().isEmpty()) {
			if (!copyFrom.getPaidCosts().stream().allMatch(cost -> cost.isPayable())) {
				return;
			}

			copyFrom.getPaidCosts().forEach(cost -> {
				spellRecord.addPaidCost(cost);
				cost.pay();
			});
		} else {
			spellRecord.addPaidCost(defaultCost);
			defaultCost.pay();
		}


		procEffects();

		super.gainExperience();
		super.recordSpellCast();
	}

	@Override
	public void startCooldown() {
		Cooldown cooldown;
		if (Objects.nonNull(copyFrom)) {
			cooldown = copyFrom.getCooldownState().toCooldown();
		} else {

			if (Objects.nonNull(customCooldown)) {
				cooldown = customCooldown;
			} else {
				cooldown = new Cooldown.Builder(SpellType.FOCUS_FIXATE)
						.ofLength(Duration.ofSeconds(3))
						.build();
			}
		}

		spellRecord.setCooldownState(new CooldownState.Builder(cooldown).build());
		cooldown.start();
	}

	@Override
	public void procEffects() {

		Apfloat spellLevel = copyFrom != null
				? (Apfloat)copyFrom.getInputParameter(RecordParameterKey.SPELL_LEVEL)
				: playerStats.get().getSpellMastery(SpellType.FOCUS_FIXATE).getLevel();

		fixateEffect = fixateEffect.coolingDown(this)
				.withDuration(Duration.ofMillis(watchDurationFormula.apply(spellLevel).longValue()));

		log.debug("Starting fixateEffect.");
		spellRecord.addParameter(RecordParameterKey.SPELL_LEVEL, spellLevel);

		fixateEffect.toggle();
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("100% Essence");
	}

	@Override
	public void castCopy(SpellCastRecord copyFrom) {
		if (Objects.isNull(copyFrom) || copyFrom.getSpellType() != SpellType.FOCUS_FIXATE) {
			throw new IllegalArgumentException("Missing copy info or incorrect spell type.");
		}

		this.copyFrom = copyFrom;
		this.cast();
	}
}
