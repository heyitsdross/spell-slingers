package com.aps.spells.entity.spell.focus;

import static org.apfloat.ApfloatMath.pow;

import java.time.Duration;
import java.util.Set;
import java.util.stream.Collectors;

import org.apfloat.Apfloat;

import com.aps.spells.altercation.ManaFlareAltercation;
import com.aps.spells.altercation.buff.MagnitudeType;
import com.aps.spells.data.Formula;
import com.aps.spells.data.save.CooldownState;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.entity.cost.ManaCost;
import com.aps.spells.entity.cost.PercentCurrentManaCost;
import com.aps.spells.entity.spell.Cooldown;
import com.aps.spells.entity.spell.Spell;
import com.aps.spells.entity.spell.SpellType;

/**
 * Nullifies other mana stores to increase void mana by a certain amount.
 */
public class FocusNegateSpell extends Spell {

	private static final ManaCost castingCost =
			new PercentCurrentManaCost(new Apfloat("100", 15), ManaColor.ESSENCE);

	private Apfloat finalNegatedMana = new Apfloat("0", 15);

	private Formula<Apfloat, Apfloat> spellPowerFormula =
			essencePaid -> new Apfloat("0.6", 15)
				.multiply(pow(essencePaid, new Apfloat("0.08", 15)));

	private Apfloat essencePaid = Apfloat.ZERO;

	@Override
	public void cast() {
		if (!isCastable()) {
			return;
		}
		super.cast();

		essencePaid = castingCost.pay();
		spellRecord.addPaidCost(castingCost);

		spellPower = spellPowerFormula.apply(essencePaid);
		spellRecord.setSpellPower(spellPower);

		procEffects();

		startCooldown();

		super.recordSpellCast();
	}

	@Override
	public boolean isCastable() {
		return castingCost.isPayable()
				|| !playerStats.get().hasCooldown(SpellType.from(this));

	}

	@Override
	public void startCooldown() {

		var cooldown = new Cooldown.Builder(SpellType.from(this))
			.ofLength(Duration.ofSeconds(9))
			.build();

		spellRecord.setCooldownState(new CooldownState.Builder(cooldown).build());
		cooldown.start();
	}

	@Override
	public void procEffects() {

		Set<ManaStore> storesToDrain = playerStats.get().getManaStores().values().stream()
				.filter(c -> !Set.of(ManaColor.VOID, ManaColor.ESSENCE).contains(c.getColor()))
				.collect(Collectors.toSet());

		for (ManaStore store : storesToDrain) {
			store.getManaAmountLock().readWriteLock(() -> {
				finalNegatedMana = finalNegatedMana.add(
						store.getAmount().multiply(new Apfloat("0.15", 15)));

				new ManaFlareAltercation(Apfloat.ZERO, store.getColor(), MagnitudeType.MULTIPLICATIVE)
					.proc();
			});
		}

		finalNegatedMana = finalNegatedMana.add(essencePaid);

		new ManaFlareAltercation(finalNegatedMana, ManaColor.VOID).proc();
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("100% current essence");
	}
}
