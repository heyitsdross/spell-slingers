package com.aps.spells.entity.spell;

import java.util.Set;

public class NullSpell extends Spell {

	@Override
	public void cast() {
	}

	@Override
	public void startCooldown() {
	}

	@Override
	public void procEffects() {
	}

	@Override
	public Set<String> getCostsReadable() {
		return Set.of("Not castable");
	}
}
