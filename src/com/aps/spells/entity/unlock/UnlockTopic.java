package com.aps.spells.entity.unlock;
import org.apfloat.Apfloat;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.entity.ManaStore;
import com.aps.spells.rx.pipeline.ContextReferential;
import com.aps.spells.rx.pipeline.UnlockUpdater;

/**
 * Determines the relevant list of topics to use for
 * unlocks and links the topic to the reactive pipeline.
 */
public enum UnlockTopic implements ContextReferential {
	ALL_SPELL_CAST_COUNT(new UnlockUpdater<Apfloat>(rxContext.get().cumulativeCasts())),
	CURRENT_CRIMSON_MANA(new UnlockUpdater<ManaStore>(playerStats.get().getManaStore(ManaColor.CRIMSON).getManaAmountSubject())),
	CURRENT_MYRTLE_MANA(new UnlockUpdater<ManaStore>(playerStats.get().getManaStore(ManaColor.MYRTLE).getManaAmountSubject())),
	CURRENT_ICTERINE_MANA(new UnlockUpdater<ManaStore>(playerStats.get().getManaStore(ManaColor.ICTERINE).getManaAmountSubject()));

	private UnlockUpdater<?> unlockUpdater;

	private UnlockTopic(UnlockUpdater<?> unlockUpdater) {
		this.unlockUpdater = unlockUpdater;
	}

	public UnlockUpdater<?> getUnlockUpdater() {
		return unlockUpdater;
	}
}
