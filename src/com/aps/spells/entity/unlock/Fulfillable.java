package com.aps.spells.entity.unlock;

public interface Fulfillable {

	boolean isFulfilled();

}
