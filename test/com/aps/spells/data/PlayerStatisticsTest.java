package com.aps.spells.data;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Test;

import com.aps.spells.main.SpellSlingerTest;

public class PlayerStatisticsTest extends SpellSlingerTest {

	@Test
	public void testIncrementCumulativeCastCountWithValidDataWorks() {
		final Apfloat before = playerStats.getCumulativeCastCount();
		playerStats.incrementCumulativeCastCount();

		assertTrue(playerStats.getCumulativeCastCount().compareTo(before) > 0);
	}

	@Test
	public void testCustomIncrementWithValidDataWorks() {
		final Apfloat before = playerStats.getCumulativeCastCount();
		playerStats.addCumulativeCasts(new Apfloat(500));

		assertTrue(playerStats.getCumulativeCastCount().compareTo(before.add(new Apfloat(500))) == 0);
	}

	@Test
	public void testCustomIncrementExpectingDifferentValueFails() {
		final Apfloat before = playerStats.getCumulativeCastCount();
		playerStats.addCumulativeCasts(new Apfloat(435));

		assertFalse(playerStats.getCumulativeCastCount().compareTo(before.add(new Apfloat(500))) == 0);
	}
}
