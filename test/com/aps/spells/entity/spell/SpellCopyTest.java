package com.aps.spells.entity.spell;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.CountDownLatch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.aps.spells.main.SpellSlingerTest;

public class SpellCopyTest extends SpellSlingerTest {

	@Test
	public void testCastCopyCreatesEntryForCopiedSpell() throws InterruptedException {
		CountDownLatch l = new CountDownLatch(1);
		new Thread(() -> {
				new EssenceGatherSpell().cast();

				while (playerStats.hasCooldown(SpellType.ESSENCE_GATHER)) {
					// Terrible spin lock to await cooldown; never use in non-test code!
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
						Assertions.fail("Thread sleep messed up.");
					}
				}

				l.countDown();
			}).start();

		l.await();

		new EssenceGatherSpell().castCopy(playerStats.getSpellHistory().get(0));

		assertTrue(playerStats.getSpellHistory().size() == 2);
	}
}
