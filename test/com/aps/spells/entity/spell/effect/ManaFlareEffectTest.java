package com.aps.spells.entity.spell.effect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Test;

import com.aps.spells.altercation.ManaFlareAltercation;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.main.SpellSlingerTest;

public class ManaFlareEffectTest extends SpellSlingerTest {

	private static final Apfloat TEST_MAGNITUDE = new Apfloat("2.5", 15);

	private static final ManaFlareAltercation effect = new ManaFlareAltercation(TEST_MAGNITUDE, ManaColor.CRIMSON);

	@Test
	public void procGivenMagnitudeRedManaCountMultipliesByGivenMagnitude() throws NoSuchFieldException, SecurityException {
		playerStats.getManaStore(ManaColor.CRIMSON).setAmount(new Apfloat("1", 15));

		effect.proc();

		assertEquals(new Apfloat("3.5", 15), playerStats.getManaStore(ManaColor.CRIMSON).getAmount());
	}

	@Test
	public void procWithValidStatsDoesNotChangeUnspecifiedManaStore() {
		playerStats.getManaStore(ManaColor.LAPIS).setAmount(new Apfloat("5"));

		effect.proc();

		Apfloat newManaCount = playerStats.getManaStores()
				.get(ManaColor.CRIMSON).getAmount();

		assertNotEquals(playerStats.getManaStore(ManaColor.LAPIS).getAmount(), newManaCount);
	}
}
