package com.aps.spells.entity.spell.effect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.aps.spells.altercation.effect.EssenceGatherAltercation;
import com.aps.spells.altercation.effect.EssenceGatherAltercation.GatherMode;
import com.aps.spells.entity.ManaColor;
import com.aps.spells.main.ConcurrencyTester;
import com.aps.spells.main.SpellSlingerTest;

public class EssenceGatherAltercationTest extends SpellSlingerTest {

	private static final EssenceGatherAltercation castEssenceGatherAltercation =
			new EssenceGatherAltercation(GatherMode.CAST);

	@BeforeEach
	public void resetManaStore() {
		playerStats.getManaStore(ManaColor.ESSENCE).setAmount(Apfloat.ZERO);
	}

	@Test
	public void procWithValidStatsObjectIncrementsByExpectedAmount() {

		Apfloat essenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

		castEssenceGatherAltercation.proc();

		Apfloat afterEssenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

		assertTrue(essenceCount.compareTo(afterEssenceCount) == -1);
	}

	@Test
	public void procWithValidStatsDoesNotDecrement() {

		Apfloat essenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

		castEssenceGatherAltercation.proc();

		Apfloat afterEssenceCount = playerStats.getManaStores()
				.get(ManaColor.ESSENCE).getAmount();

		assertTrue(essenceCount.compareTo(afterEssenceCount) <= 0);
	}

	/**
	 * Tests concurrency bug of EssenceGatherEffect
	 */
	@Test
	public void multithreadedProcClickEffectAlwaysProducesExpectedResult() throws InterruptedException {
		playerStats.getManaStore(ManaColor.ESSENCE).setCap(new Apfloat("1000000000", 15));
		playerStats.getManaStore(ManaColor.ESSENCE).setAmount(Apfloat.ZERO);

		new ConcurrencyTester(() -> {
			  new EssenceGatherAltercation(GatherMode.CLICK).proc();
			  new EssenceGatherAltercation(GatherMode.CLICK).proc();
			  }, 500)
		.test(() -> assertEquals(5250, playerStats.getManaStore(ManaColor.ESSENCE).getAmount().longValue()));

		playerStats.getManaStore(ManaColor.ESSENCE).setCap(new Apfloat("200", 15));
	}

	/**
	 * Tests concurrency bug of EssenceGatherEffect
	 */
	@Test
	public void multithreadedProcMixedEffectsAlwaysProducesExpectedResult() throws InterruptedException {
		playerStats.getManaStore(ManaColor.ESSENCE).setCap(new Apfloat("1000000000", 15));
		playerStats.getManaStore(ManaColor.ESSENCE).setAmount(Apfloat.ZERO);

		new ConcurrencyTester(() -> {
			  new EssenceGatherAltercation(GatherMode.CLICK).proc();
			  new EssenceGatherAltercation(GatherMode.CAST).proc();
			  new EssenceGatherAltercation(GatherMode.CLICK).proc();
			  }, 500)
		.test(() -> assertEquals(12750, playerStats.getManaStore(ManaColor.ESSENCE).getAmount().longValue()));

		playerStats.getManaStore(ManaColor.ESSENCE).setCap(new Apfloat("200", 15));
	}
}
