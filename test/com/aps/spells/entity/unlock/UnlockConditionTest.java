package com.aps.spells.entity.unlock;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.function.Function;
import java.util.function.Supplier;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.aps.spells.entity.ManaColor;
import com.aps.spells.main.SpellSlingerTest;

public class UnlockConditionTest extends SpellSlingerTest {

	private static final Function<Apfloat, Boolean> castCountChecker = spellCastCount ->  {
			return new Apfloat("25", 15L).compareTo(spellCastCount) < 0;
	};

	private static final Function<Apfloat, Boolean> redManaChecker = redManaCount ->  {
			return new Apfloat("65", 15L).compareTo(redManaCount) < 0;
	};

	@Test
	public void testCheckUnlockConditionWithProperLogicWorksCorrectly() {
		Supplier<Apfloat> statGetter = () -> playerStats.getCumulativeCastCount();

		UnlockCondition<Apfloat> spellCastUnlockCondition = new UnlockCondition.Builder<Apfloat>(UnlockTopic.ALL_SPELL_CAST_COUNT, castCountChecker,
				statGetter).build();

		playerStats.addCumulativeCasts(new Apfloat("26", 15L));

		Assertions.assertTrue(spellCastUnlockCondition.check());
	}

	@Test
	public void testCheckUpdatesWithUpdatedValues() {
		Supplier<Apfloat> statGetter = () -> playerStats.getManaStore(ManaColor.CRIMSON).getAmount();

		UnlockCondition<Apfloat> unlockCondition =
				new UnlockCondition.Builder<Apfloat>(UnlockTopic.CURRENT_CRIMSON_MANA, redManaChecker, statGetter)
					.build();

		playerStats.getManaStore(ManaColor.CRIMSON).setAmount(new Apfloat("68", 15L));

		assertTrue(unlockCondition.check());

		playerStats.getManaStore(ManaColor.CRIMSON).setAmount(new Apfloat("38", 15L));

		assertFalse(unlockCondition.check());
	}
}
