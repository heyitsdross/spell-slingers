package com.aps.spells.entity;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Test;

import com.aps.spells.entity.unlock.UnlockTopic;
import com.aps.spells.main.SpellSlingerTest;

public class ContentLockTest extends SpellSlingerTest {

	@Test
	public void testContentLockUpdatingWithFulfilledConditionTriggersUnlock() throws InterruptedException {
		ContentLock unlockable = new ContentLock.Builder()
				.withCondition(
						UnlockTopic.ALL_SPELL_CAST_COUNT,
						cumCastCount -> new Apfloat(100).compareTo(cumCastCount) <= 0,
						() -> playerStats.getCumulativeCastCount())
				.build();

		playerStats.addCumulativeCasts(new Apfloat("101", 15L));

		Thread.sleep(100L); // Wait out other threads.

		assertTrue(unlockable.isUnlocked());
	}

	@Test
	public void testContentLockUpdatingWithNoFulfilledConditionStaysLocked() throws InterruptedException {
		ContentLock unlockable = new ContentLock.Builder()
				.withCondition(
						UnlockTopic.ALL_SPELL_CAST_COUNT,
						cumCastCount -> new Apfloat(100).compareTo(cumCastCount) <= 0,
						() -> playerStats.getCumulativeCastCount())
				.build();

		playerStats.addCumulativeCasts(new Apfloat("99", 15L));

		Thread.sleep(100L);  // Wait out other threads.

		assertFalse(unlockable.isUnlocked());
	}
}
