package com.aps.spells.main;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;

import com.aps.spells.data.PlayerStatistics;

/**
 * Common test class that contains common initialization code necessary for most
 * other tests to function.
 */
@ExtendWith(ApplicationExtension.class)
public abstract class SpellSlingerTest {

	protected static PlayerStatistics playerStats;

	@BeforeAll
	public static void setUp() {
		playerStats = GameContext.get().getSaveFileManager().createPlayerStatistics();

		GameContext.get().setPlayerStats(playerStats);
	}

	@AfterAll
	public static void tearDown() {
		playerStats = null;
		GameContext.get().setPlayerStats(null);
	}
}
