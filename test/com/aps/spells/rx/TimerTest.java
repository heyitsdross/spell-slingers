package com.aps.spells.rx;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.apfloat.Apfloat;
import org.junit.jupiter.api.Test;

import com.aps.spells.main.SpellSlingerTest;
import com.aps.spells.rx.Timer;

public class TimerTest extends SpellSlingerTest {

	@Test
	public void getElapsedTimeReturnsApfloatWithValueInMillis() {
		Timer t = new Timer.Builder(Duration.ofSeconds(3))
				.startingAt(Duration.ofMillis(200))
				.build();

		assertEquals(new Apfloat("200", 15), t.getTimeElapsed());
	}

	@Test
	public void getElapsedTimeAsDurationReturnsTickNumber() {
		Timer t = new Timer.Builder(Duration.ofSeconds(3))
				.startingAt(Duration.ofMillis(800))
				.build();

		assertEquals(800, t.getTimeElapsed(ChronoUnit.MILLIS).toMillis());
	}

	@Test
	public void getRemainingTicksReturnsCorrectTickNumber() {
		Timer t = new Timer.Builder(Duration.ofMillis(3000))
				.startingAt(Duration.ofMillis(800))
				.build();

		assertEquals(22, t.getRemainingTicks().longValue());
	}
}
